variable "service" {
  description = "The service this Autoscale Group is for, will be interpolated at position 2 in the name"
  type = "string"
}

variable "zone" {
  description = "The zone this Autoscale Group should be created in"
  type = "string"
}

variable "short_zone" {
  description = "The short zone name, will be interpolated at position 3 in the name"
  type = "string"
}

variable "target" {
  description = "The full URL to the Instance Group Manager which this Autoscale Group will control"
  type = "string"
}

variable "min" {
  description = "The desired minimum count of instances this Autoscale Group should maintain"
  type = "string"
  default = 1 
}

variable "max" {
  description = "The desired maximum count of instances this Autoscale Group should maintain"
  type = "string"
  default = 2 
}

variable "cooldown" {
  description = "The time (seconds) until in between autoscale events"
  type = "string"
  default = 600 
}

variable "utilisation" {
  description = "Target CPU utilisation before an autoscale event will occur"
  type = "string"
  default = 0.9 
}
