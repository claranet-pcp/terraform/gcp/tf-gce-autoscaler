resource "google_compute_autoscaler" "autoscaler" {
  name = "autoscaler-${var.service}-${var.short_zone}"
  zone = "${var.zone}"
  target = "${var.target}"
  autoscaling_policy = {
    min_replicas = "${var.min}"
    max_replicas = "${var.max}"
    cooldown_period = "${var.cooldown}"
    cpu_utilization = {
      target = "${var.utilisation}"
    }
  }
}
